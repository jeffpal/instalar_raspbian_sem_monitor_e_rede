Este repositório foi criado para ajudar desenvolvedores brasileiros, ou falantes de língua portuguesa, a instalar, configurar e usar o Raspbian no Raspberry Pi, sem monitor Hdmi e rede.

__________________________________________________________________________________
Material utilizado
__________________________________________________________________________________

1 - Raspberry Pi 3 + Cartão SD (mínimo 2GB) + Fonte de alimetação;

2 - computador com Ubuntu 16.04;

3 - cabo de rede ethernet comum;

__________________________________________________________________________________
Fotmatar o cartão SD Card
__________________________________________________________________________________

Caso o seu cartão de memória não seja novo e já contenha arquivos e/ou partições, é recomendável que ele seja formatado e que suas partiçoes sejam excluídas. Embora a formatação seja recomendada, em alguns casos, a instalação pode funcior sem a sua necessidade.


Windows:

1 - Baixe e instale o SD Card Formatter (https://www.sdcard.org/downloads/formatter_4/eula_windows/index.html);

2 - Formate o seu Cartão SD utilizando o SD Card Formatter ou o próprio formatador do Windows.


Linux:

1 - Baixe e instale o Gparted ou faça isso pelo Ubuntu Software;

2 - Exclua todas as partições.

__________________________________________________________________________________
Gravar a imagem do Raspbian
__________________________________________________________________________________

1 - Baixe o arquivo 2017-04-10-raspbian-jessie-lite.zip, contido neste repositório.


Windows:

1 - Baixe e instale o Win32DiskImager (https://sourceforge.net/projects/win32diskimager/) ou o Etcher(https://etcher.io/);

2 - descompacte o arquivo 2017-04-10-raspbian-jessie-lite.zip e grave a imagem 2017-04-10-raspbian-jessie-lite.img no seu cartão de memória utilizando o Win32DiskImager ou o Etcher.

3 - concluída a gravação com sucesso, retire o cartão e insira-o ao Raspberry, conecte o cabo de rede ao Rasp e ao computador e ligue o dispositivo. 


Linux:

1 - Baixe o Etcher (https://etcher.io/)

2 - grave a imagem do arquivo 2017-04-10-raspbian-jessie-lite.zip no seu cartão de memória utlizando o Etcher. Obs. não é necessário descompactar o arquivo, pois o Etcher pode ler a imagem a partir do arquivo .zip.

3 - concluída a gravação com sucesso, retire o cartão e insira-o ao Raspberry, conecte o cabo de rede ao Rasp e ao computador e ligue o dispositivo.

__________________________________________________________________________________
Configurações de rede
__________________________________________________________________________________

1 - Na barra de ferramentas do Ubuntu clique no ícone de rede -> Edit connections...

2 - na janela Network Connections clique em -> Add 

3 - crie uma conexão do tipo Ethernet com o nome desejado;

4 - na aba IPv4 Settings -> Method, selecione Shared to other computers (compartilhado com outros computadores).

5 - salve a conexão criada;

6 - no ícone de rede selecione a concexão criada. 

7 - no terminal:
	
com o raspberry ligado e conectado ao computador através do cabo de rede, execute o seguinte comando: 
	
	cat /var/lib/misc/dnsmasq.leases

Esse comando deve retornar algo semelhante a: 
	
	1493565363 b8:27:eb:30:c4:91 10.42.0.132 raspberrypi 01:b8:27:eb:30:c4:91
	
observe o endereço IP referente a raspberrypi

8 - acessar o raspberry via ssh

para conectar-se ao raspberry pi via ssh utilize o ip observado anteriormente e execute o seguinte comando:
	
	ssh pi@10.42.0.132

caso a conexão ssh funcione, será solicitada a senha do host pi@10.42.0.132.
	
	pi@10.42.0.132's password: 

senha do usuário pi: raspberry

	
PRONTO! 
VOCÊ ACABA DE INSTALAR O RASPBIAN E ACESSAR O RASPBERRY PI SEM UMA REDE E UM MONITOR CONECTADOS AO RASP. SEJA FELIZ!

Referências

http://www.aoakley.com/articles/2016-12-05-raspbian-enable-ssh.php